package com.example.dragonball.ui.main

import com.example.dragonball.ui.list.Personaje
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.dragonball.R

/**
 * Clase principal
 * Esta clase inicia la funcionalidad de la app
 */
class MainActivity : AppCompatActivity() {

    /**
     * Metodo onCreate, este metodo inicializa la app vinculando su vista xml
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main);

        val textViewWelcome=findViewById<TextView>(R.id.textViewWelcome);
        val buttonAction =findViewById<Button>(R.id.buttonAction);

        //listener cuando se toque el boton se vaya a otro activity
        buttonAction.setOnClickListener{
            val intent = Intent(this, Personaje::class.java);
            startActivity(intent);
        }
    }
}