package com.example.dragonball.ui.list

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.example.dragonball.data.api.RetroFitClient
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dragonball.R
import com.example.dragonball.data.model.PersonajesModel
import com.example.dragonball.ui.adapters.PersonajesAdapter

class Personaje : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView;
    private lateinit var personajesAdapter: PersonajesAdapter;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personajes);

        recyclerView = findViewById(R.id.recycler_view_personajes);
        recyclerView.layoutManager = LinearLayoutManager(this);

        // Inicializar el proceso de carga de datos
        //fetchCharacters();

        // Iniciar la llamada de Retrofit
        RetroFitClient.instance.obtienePersonajes().enqueue(object : Callback<PersonajesModel> {
            override fun onResponse(call: Call<PersonajesModel>, response: Response<PersonajesModel>) {
                if (response.isSuccessful) {
                    response.body()?.let { personajesModel ->
                        personajesAdapter = PersonajesAdapter(personajesModel.items) // Aquí pasas la lista de items
                        recyclerView.adapter = personajesAdapter
                    }
                } else {
                    Log.e("PersonajesActivity", "Failed to fetch characters: ${response.errorBody()?.string()}")
                }
            }

            override fun onFailure(call: Call<PersonajesModel>, t: Throwable) {
                Log.e("PersonajesActivity", "Failed to fetch characters: ${t.message}")
            }
        })
    }
}

