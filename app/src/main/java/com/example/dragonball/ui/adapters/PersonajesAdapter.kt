package com.example.dragonball.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.dragonball.R
import com.example.dragonball.data.model.Item

class PersonajesAdapter(private val items: List<Item>) : RecyclerView.Adapter<PersonajesAdapter.PersonajeViewHolder>() {

    class PersonajeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.findViewById(R.id.name_text_view)
        val descriptionTextView: TextView = view.findViewById(R.id.description_text_view)
        val imageView: ImageView = view.findViewById(R.id.image_view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonajeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_personaje, parent, false)
        return PersonajeViewHolder(view)
    }

    override fun onBindViewHolder(holder: PersonajeViewHolder, position: Int) {
        val personaje = items[position]
        holder.nameTextView.text = personaje.name
        holder.descriptionTextView.text = personaje.description
        // Cargar la imagen del personaje con Glide
        Glide.with(holder.imageView.context)
            .load(personaje.image)
            .placeholder(R.drawable.placeholder) // imagen de placeholder mientras se carga la imagen real
            .error(R.drawable.error_image)       // imagen mostrada en caso de error al cargar la imagen real
            .into(holder.imageView)
    }

    override fun getItemCount() = items.size
}