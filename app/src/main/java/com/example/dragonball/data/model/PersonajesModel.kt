package com.example.dragonball.data.model

data class PersonajesModel(
    val items: List<Item>,
    val links: Links,
    val meta: Meta
)