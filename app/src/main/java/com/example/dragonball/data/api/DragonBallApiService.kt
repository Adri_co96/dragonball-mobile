package com.example.dragonball.data.api

import com.example.dragonball.data.model.PersonajesModel
import retrofit2.Call
import retrofit2.http.GET

/**
 * @author aCortes
 * Esta clase funciona como interfaz para la solicitud de request http a la api de dragonball
 */
interface DragonBallApiService {

    //Metodo no implementado para obtener los personajes
    @GET("characters")
    fun obtienePersonajes(): Call<PersonajesModel>
}