package com.example.dragonball.data.model

data class Links(
    val first: String,
    val last: String,
    val next: String,
    val previous: String
)